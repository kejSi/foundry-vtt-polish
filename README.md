# Polish language support for Foundry VTT

## Installation

1. Copy this link and use it in Foundrys module manager to install the module:
    
    > https://gitlab.com/kejSi/foundry-vtt-polish/-/raw/master/module.json

2. Enable the module in your worlds module settings
3. Choose "Polish" as language in your world settings

